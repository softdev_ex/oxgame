/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onanong.game_ox;

import java.util.Scanner;


/**
 *
 * @author Administrator
 */

public class ox {
    static char XO[][] = {{'-','-','-'},
		{'-','-','-'},
		{'-','-','-'}};
    static char player = 'X';
    static int row, col;
    static int round = 0;
    public static void main(String[] args) {
        System.out.println("Welcome to OX Game");
        Board();
        while (true) {
            inputPos();
            if(checkWinner()) {
                break;
            }
            if(checkDraw()) {
                break;
            }
            switchPlayer();
        }   
    }
    public static void Board() {
        System.out.println("  1" + " 2" + " 3");
        for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
                            System.out.print(" ");    
                            System.out.print(XO[i][j]);
			}
			System.out.println();
		}

    }
     public static void inputPos() {
        Scanner kb = new Scanner(System.in);
        while(true) {
            System.out.println(player + " turn");
            System.out.println("please input Row col: ");
                int row = kb.nextInt();
                int col = kb.nextInt();
                if(row < 0 || row > 3 || row == 0 || col < 0 || col > 3 || col == 0) {
                    System.out.println("Row and Column must be 1-3");
                    Board();
                    continue;	
                }
                
                row = row - 1;
		col = col - 1;
                if (XO[row][col] == 'X' || XO[row][col] == 'O') {
                    System.out.println("Position filled. Try again");
                    Board();
                    continue;	
		}
		XO[row][col] = player;
                Board();
                round++;
                break;      
	}
     }
     
     public static void switchPlayer() {
	if (player == 'X') {
            player = 'O';
	} else {
            player = 'X';
	}
    }
     
    public static boolean checkWinner () {
        boolean winner = false;
        for (int i = 0; i < XO.length; i++ ) {
            if (XO[0][i] == player && XO[1][i] == player && XO[2][i] == player ){
                System.out.println("Player " + player + " WIN...");
                System.out.print("Bye bye...");
		return true ;
            }
        }
        for (int i = 0; i < XO.length; i++ ) {
            if (XO[i][0] == player && XO[i][1] == player && XO[i][2] == player ){
                System.out.println("Player " + player + " WIN...");
                System.out.print("Bye bye...");
		return true ;
            }
        }
        if (XO[0][2] == player && XO[1][1] == player && XO[2][0] == player ){
                System.out.println("Player " + player + " WIN...");
                System.out.print("Bye bye...");
		return true ;
        }
        if (XO[0][0] == player && XO[1][1] == player && XO[2][2] == player ){
                System.out.println("Player " + player + " WIN...");
                System.out.print("Bye bye...");
		return true ;
        }
        return false ;
    }
    
    public static boolean checkDraw(){
        boolean draw = false;
        if(round == 9) {
            System.out.println("Draw...");
            System.out.print("bye bye...");
			return true ;
        }
        return false ;
     } 
}
